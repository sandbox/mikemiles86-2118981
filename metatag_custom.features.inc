<?php
/**
 * @file
 * Features file for the metatag_custom module.
 */

/**
 * Implements hook_features_api().
 */
function metatag_custom_features_api() {
  return array(
    'metatag_custom' => array(
      'name'            => t('Custom Metatags'),
      'default_hook'    => 'metatag_custom_export_tags',
      'feature_source'  => TRUE,
      'default_file'    => FEATURES_DEFAULTS_INCLUDED,
      'file'            => drupal_get_path('module', 'metatag_custom') . '/metatag_custom.features.inc',
    ),
  );
}

/**
 * Implements COMPONENT_features_export_options().
 *
 * Inform features about the available metatags in the database.
 */
function metatag_custom_features_export_options() {
  return db_select('metatag_custom', 'cmt')
    ->fields('cmt', array('name', 'name'))
    ->execute()
    ->fetchAllKeyed();
}

/**
 * Implements COMPONENT_features_export().
 *
 * Process the features export array for metatags.
 */
function metatag_custom_features_export($data, &$export, $module_name) {
  $export['dependencies']['metatag_custom'] = 'metatag_custom';

  foreach ($data as $component) {
    $export['features']['metatag_custom'][$component] = $component;
  }

  return array();
}


/**
 * Implements COMPONENT_features_export_render().
 *
 * Render workbench metatags as code.
 */
function metatag_custom_features_export_render($module_name, $data) {
  $items = array();
  foreach ($data as $schedule) {
    $items[$schedule] = metatag_custom_load_tags($schedule);
  }

  $code = "  \$items = " . features_var_export($items, '  ') . ";\n";
  $code .= '  return $items;';

  return array('metatag_custom_export_tags' => $code);
}


/**
 * Implements COMPONENT_features_revert().
 */
function metatag_custom_features_revert($module) {
  metatag_custom_features_rebuild($module);
}

/**
 * Implements COMPONENT_features_enable_feature().
 */
function metatag_custom_features_enable_feature($module) {
  metatag_custom_features_rebuild($module);
}

/**
 * Implements COMPONENT_features_rebuild().
 *
 * Store each exported schedule in the database.
 */
function metatag_custom_features_rebuild($module) {
  $defaults = features_get_default('metatag_custom', $module);
  foreach ($defaults as $tag) {
    metatag_custom_save_tag($tag['name'], $tag);
  }
  drupal_static_reset('metatag_custom_load_tags');
}
