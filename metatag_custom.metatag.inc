<?php

/**
 * @file
 * Provides metatag functions for Custom Metatags.
 */

/**
 * Implements hook_metatag_info().
 */
function metatag_custom_metatag_info() {

  module_load_include('module', 'metatag_custom');
  //define custom grouping
  $info['groups']['metatag_custom'] = array(
    'label' => t('Custom'),
    'form' => array(
      '#weight' => 200,
    ),
  );
  //attempt to retrieve custom metatags
  if ($custom_tags = metatag_custom_load_tags()) {
    //for each tag, add data info array
    foreach ($custom_tags as $tag_name => $tag_data) {
      $info['tags'][$tag_data['metaname']] = array(
        'label'       => t($tag_data['label']),
        'description' => t($tag_data['description']),
        'class'       => $tag_data['class'],
        'group'       => $tag_data['group_name'],
      );
    }
  }

  return $info;
}

/**
 * Implements hook_metatag_config_default_alter().
 */
function metatag_custom_metatag_config_default_alter(array &$configs) {
  module_load_include('module', 'metatag_custom');
  //attempt to retrieve custom metatags
  if ($tags = metatag_custom_load_tags()) {
    $tag_configs = array();
    //for each tag
    foreach ($tags as $tag_name => $tag_data) {
      //for each metadata config
      foreach ($configs as &$config) {
        //add tag default value to config.
        $config->config += array(
          $tag_data['metaname'] => array('value' => $tag_data['default_val']),
        );
      }
    }
  }

}
