<?php


/**
 * @file
 * Provides admin functions for Custom Metatags.
 */

/**
 * display admin table to manage custom metatags
 * @return string rendered page HTML
 */
function metatag_custom_admin_tags() {
  //build a table to show the different metatags
  $headers = array(
    array('data' => t('Label')),
    array('data' => t('Name Attribute')),
    array('data' => t('description')),
    array('data' => t('Default Value')),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $rows = array();
  module_load_include('module', 'metatag_custom');
  //attempt to fetch custom metatags
  if ($metatag_custom = metatag_custom_load_tags()) {
    //loop through the tags and add to the table rows
    foreach ($metatag_custom as $tag_name => $tag_data) {
      $rows[] = array(
        $tag_data['label'],
        $tag_data['metaname'],
        $tag_data['description'],
        $tag_data['default_val'],
        l(t('Edit'), 'admin/config/search/metatags/custom/' . $tag_name . '/edit'),
        l(t('Delete'), 'admin/config/search/metatags/custom/' . $tag_name . '/delete'),
      );
    }
  }
  //no tags found, diplay message
  else {
    $rows[] = array(array('data' => t('No Custom Metatags'), 'colspan' => 6));
  }

  //add a row for a link to add a new schedule
  $rows[] = array(
    array(
      'data'    => l(t('Add Tag'), 'admin/config/search/metatags/custom/add'),
      'colspan' => 6,
    )
  );
  //returned the themed table
  return theme('table', array('header' => $headers, 'rows' => $rows));
}


/**
 * form to edit/create custom metatags
 * @param  array  $form       form array
 * @param  array  $form_state form_state array
 * @param  string $tag_name   tag machine name (optional)
 * @return form              a form object
 */
function metatag_custom_admin_edit_tag($form, &$form_state, $tag_name=FALSE) {

  //the label field
  $form['label'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Tag Label'),
    '#description'  => t('Administrative Label for this tag.'),
    '#required'     => TRUE,
    '#size'         => 40,
    '#maxlength'    => 255,
  );

  //the machine name field
  $form['name'] = array(
    '#type'         => 'machine_name',
    '#title'        => t('Machine Name'),
    '#description'  => t('The unique machine readable name for this tag.'),
    '#required'     => TRUE,
    '#size'         => 40,
    '#maxlength'    => 255,
    '#machine_name'   => array(
      'exists' => '_metatag_custom_check_machine_name_exists',
      'source' => array('label'),
    ),
  );

  //the meta name field
  $form['metaname'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Meta Name'),
    '#description'  => t('Value for the "name" attribute of the metatag.'),
    '#required'     => TRUE,
    '#size'         => 40,
    '#maxlength'    => 255,
  );

  //the description field
  $form['description'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Tag Description'),
    '#description'  => t('Administrive description for this tag'),
    '#size'         => 40,
    '#maxlength'    => 255,
  );

  //the default value field
  $form['default'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Default Value'),
    '#description'  => t('Default Value for this tag. Can be text or one of the available tokens'),
    '#size'         => 40,
    '#maxlength'    => 255,
  );

  // Show the list of available tokens. for default value settings
  $form['tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => token_get_entity_mapping(),
    '#dialog' => TRUE,
  );

  $form['actions']['#type'] = 'actions';

  $form['actions']['submit'] = array(
    '#type'   => 'submit',
    '#value' => t('Save'),
  );

  //if editing an existing tag, load the default values for the fields
  if ($tag_name && ($tag = metatag_custom_load_tags($tag_name))) {
    $form['#tag']                           = $tag;
    $form['label']['#default_value']        = $tag['label'];
    $form['metaname']['#default_value']     = $tag['metaname'];
    $form['description']['#default_value']  = $tag['description'];
    $form['default']['#default_value']      = $tag['default_val'];
    $form['actions']['submit']['#value']    = t('Update');
    $form['actions']['delete']              = array(
      '#type'   => 'submit',
      '#value'  => t('Delete'),
    );
    //remove hte machine name so it cannot be edited
    unset($form['name']);
  }

  return $form;
}

/**
 * validate function for tag edit/add form
 * @param  array $form       the form array
 * @param  array $form_state the form_state arrat
 * @return none
 */
function metatag_custom_admin_edit_tag_validate($form, &$form_state) {
  //validate label
  if (strlen(trim(strip_tags($form_state['values']['label'])))<1) {
    form_set_error('label', t('Invalid Tag name provided'));
  }

  //validate metaname
  if (strlen(trim(strip_tags($form_state['values']['metaname'])))<1) {
    form_set_error('metaname', t('Invalid metaname provided'));
  //check that is is unique
  }
  else {
    //check to see if metaname already exists
    $metatag_name = _metatag_custom_get_tag_name_from_metaname(trim(strip_tags($form_state['values']['metaname'])));

    $different_tag = ($metatag_name && isset($form['#tag']) && ($form['#tag']['name']!=$metatag_name));
    $not_new = (!isset($form['#tag']) && $metatag_name);

    if ($different_tag || $not_new) {
      form_set_error('metaname', t('Metatag with that name attribute already exists'));
    }
  }
}

/**
 * submit function for add/edit tag form
 * @param  array $form       the form array
 * @param  array $form_state the form_sate array
 * @return none
 */
function metatag_custom_admin_edit_tag_submit($form, &$form_state) {
  //check to see if the delete button was clicked
  if ($form_state['clicked_button']['#id'] == 'edit-delete') {
    //redirect to the delete form
    drupal_goto('admin/config/search/metatags/custom/delete/' . $form['#tag']['tid']);
  //only other button is the submit button
  }
  else {
    //build the array of tag data
    $tag_data = array(
      'label'       => trim(strip_tags($form_state['values']['label'])),
      'description' => trim(strip_tags($form_state['values']['description'])),
      'default'     => trim(strip_tags($form_state['values']['default'])),
      'metaname'    => trim(strip_tags($form_state['values']['metaname'])),
    );

    //updating an existing tag?
    if (isset($form['#tag'])) {
      //fetch the machine name form the object
      $machine_name = $form['#tag']['name'];
    }
    else {
      //fetch the new machine name from passed values
      $machine_name = $form_state['values']['name'];
    }

    module_load_include('module', 'metatag_custom');
    //attempt a save/update
    if ($result = metatag_custom_save_tag($machine_name, $tag_data)) {
      drupal_set_message(t('Tag ' . ($result==1 ? 'Saved':'Updated')), 'status', FALSE);
      //redirect back to the schedules table
      $form_state['redirect'] = 'admin/config/search/metatags/custom';
    }
    else {
      drupal_set_message(t('Error saving tag'), 'error', FALSE);
    }
  }
}

/**
 * confirmation form for deleteing a custom meta tag
 * @param  array $form       the form array
 * @param  array $form_state the form_state arraty
 * @param  srting $name       the machine name of an existing custom metatag
 * @return array             a form object
 */
function metatag_custom_admin_delete_tag($form, &$form_state, $name) {
  //attempt to load the tag
  if ($tag = metatag_custom_load_tags($name)) {
    //store to form
    $form['#tag'] = $tag;
    //build confirmation form
    return confirm_form($form,
      t('Are you sure you want to delete the custom metatag "' . $tag['label'] . '"?'),
      'admin/config/search/metatags/custom',
      t('This action cannot be undone')
    );
  }
  //unable to load form, not sure what trying to delete.
  else {
    drupal_set_message(t('Invalid Tag machine name'), 'error', FALSE);
    //send back to tags table
    drupal_goto('admin/config/search/metatags/custom');
  }
}

/**
 * submit function for tag delete form
 * @param  array $form       the form array
 * @param  array $form_state the form_state array
 * @return none
 */
function metatag_custom_admin_delete_tag_submit($form, &$form_state) {
  $tag = $form['#tag'];
  module_load_include('module', 'metatag_custom');
  if (metatag_custom_delete_tags($tag['name'])) {
    drupal_set_message(t('Custom Metatag deleted'), 'status', FALSE);
    //go back to schedules page
    $form_state['redirect'] = 'admin/config/search/metatags/custom';
  }
  //unable to delete, show error message.
  else {
    drupal_set_message(t('Error deleting custom metatag'), 'error', FALSE);
  }
}



